
<?php
  include 'db_conection.php';
  include 'header.php';
 ?>
<div class="products">
    <?php
      $sql = "SELECT * FROM pakabukai";
      $result = $mysqli->query($sql);
      while($array = $result->fetch_assoc()):
    ?>
     <div class="product-item">
       <button data-product = "foto<?php echo $array['id']; ?>" class="zoom"><strong>+</strong></button>
       <img src="assets/images/geresnes_foto/pakabukai/<?php echo $array['image']; ?>" alt="<?php echo $array['name']; ?>">
       <h5><?php echo $array['name'];?></h5>
     </div>
     <div class="issoksta foto<?php echo $array['id']; ?>">
       <div class="pagrindas">
       </div>
       <div class="cover">
         <img src="assets/images/geresnes_foto/pakabukai/<?php echo $array['image']; ?>" alt="<?php echo $array['name']; ?>">
         <p><?php echo $array['description']; ?> </p>
       </div>
     </div>
    <?php
    endwhile;
    ?>
</div>
 <?php
   include 'footer.php';
  ?>
