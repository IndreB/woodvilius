<footer>
  <div class="footer">
    <a href="https://www.facebook.com/pinoccionose/" target="_blank"><i class="fa fa-facebook-square" style="font-size:36px"></i></a>
    <a href="#"><i class="fa fa-linkedin-square" style="font-size:36px"></i></a>
    <a href="#"><i class="fa fa-youtube-square" style="font-size:36px"></i></a>
  </div>
</footer>
  <script type="text/javascript" src="assets/js/jquery-3.3.1.min.js"></script>
  <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="assets/js/scripts.js"></script>
</body>
</html>
