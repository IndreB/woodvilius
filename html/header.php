<!DOCTYPE html>
<html lang="en">
<head>
  <link rel="stylesheet" href="assets/css/reset.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="assets/css/index-styles.css">
  <title>WOOD VILIUS</title>
</head>
<body>
  <header>
    <div class="header-menu-item">
      <a href="index.php">WOOD VILIUS</a>
    </div>
    <ul class="nav">
      <li class="header-menu-item has-dropdown">
        <a class="menubar" href="#">MENIU <span>>></span></a>
        <ul class="dropdown">
          <li class="menu-item"><a href="pakabukai.php">PAKABUKAI</a></li>
          <li class="menu-item"><a href="paveikslai.php">PAVEIKSLAI</a></li>
          <li class="menu-item"><a href="pakabukai.php">SKULPTURĖLĖS</a></li>
          <li class="menu-item"><a href="pakabukai.php">ŠVIESTUVAI</a></li>
          <li class="menu-item"><a href="pakabukai.php">KITA</a></li>
        </ul>
      </li>
      <li class="header-menu-item">
        <a href="index.php">titulinis</a>
      </li>
      <li class="header-menu-item">
        <a href="index.php#about">APIE</a>
      </li>
      <li class="header-menu-item">
        <a href="index.php#contacts">KONTAKTAI</a>
      </li>
      <li class="header-menu-item">
        <a href="index.php#message">SUSISIEKTI</a>
      </li>
    </ul>
    <div class="burger">
      <div></div>
      <div></div>
      <div></div>
    </div>
  </header>
