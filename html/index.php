<?php
  include ('header.php');
 ?>
  <main>
    <div class="main-meniu">
      <div class="empty"><div></div></div>
      <div class="main-meniu-item">
        <div>
        <a href="pakabukai.php">PAKABUKAI</a>
        </div>
      </div>
      <div class="empty"><div></div></div>
      <div class="main-meniu-item">
        <div>
        <a href="paveikslai.php">PAVEIKSLAI</a>
        </div>
      </div>
      <div class="empty"><div></div></div>
      <div class="main-meniu-item">
        <div>
        <a href="pakabukai.php">SKULPTURĖLĖS</a>
        </div>
      </div>
      <div class="empty"><div></div></div>
      <div class="main-meniu-item">
        <div>
        <a href="pakabukai.php">ŠVIESTUVAI</a>
        </div>
      </div>
      <div class="empty"><div></div></div>
      <div class="main-meniu-item">
        <div>
        <a href="pakabukai.php">KITA</a>
        </div>
      </div>
    </div>
    <div class="about" id="about">
      <div>
        <p>
          Everything starts from little things – from a walk in the park,
          from a branch of a tree, from  the sand from the beach runing through my
          fingers... Everything starts from small idea and huge desire to make it real.
        </p>
        <p>
          All my creativity was born from big love to simplicity, naturalnessand warmth.
        </p>
        <p>
          I wish that my works of art decorate and gives warmth to you and your home
          the same way they gave a lot of joy and great satisfaction while making them.
        </p>
      </div>
    </div>
    <div id="contacts" class="contact">
      <div class="map">
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d9079.584472001296!2d25.306037244843306!3d55.32490008705581!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46e78b199bdf662f%3A0x8e591b52d51a316a!2zUGFrcnnFvsSXIDMzMzE3!5e0!3m2!1slt!2slt!4v1516367447709" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
      </div>
      <div class="contacts-bord">
        <ul>
          <li>
            <p>ADRESAS:</p>
            <P>Pakryžės km., Alantos sen., Molėtų raj. LT-33317</P>
          </li>
          <li>
            <p>TEL. NUMERIS:</p>
            <P>+37068498070</P>
          </li>
          <li>
            <p>E. PAŠTAS:</p>
            <p>pinokiwoodcraft@gmail.com</p>
          </li>
        </ul>
      </div>
    </div>
    <form id="message" class="message-box" action="index.php" method="post">
        <div><input type="text" name="name" value="" placeholder="  Įveskite savo vardą"></div>
        <div><input type="text" name="email" value="" placeholder="  Įveskite el.pašto adresą"></div>
        <div><textarea name="message" rows="1" cols="80" placeholder=" Rašykite žinutę"></textarea></div>
        <div><button class="button">Siųsti</button></div>
    </form>
    <?php
        // $to = "indre.balseviciute@gmail.com"; // this is your Email address
        // $from = $_POST['email']; // this is the sender's Email address
        // $first_name = $_POST['name'];
        // $subject = "Form submission";
        // $subject2 = "Copy of your form submission";
        // $message = $name . " wrote the following:" . "\n\n" . $_POST['message'];
        // $message2 = "Here is a copy of your message " . $name . "\n\n" . $_POST['message'];
        //
        // $headers = "From:" . $from;
        // $headers2 = "From:" . $to;
        // if(!mail($to, $subject, $message, $headers)){
        //
        //  echo "Error !!";
        //
        // }else{
        //
        //  echo "Email Sent !!";
        //
        // }
    ?>
  </main>
  <?php
    include 'footer.php';
   ?>
