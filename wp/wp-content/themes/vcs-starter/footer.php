
</main>
<footer>
  <?php
  if( have_rows('fo_footer','option') ):?>
    <div class="footer">
      <?php
      $link = get_sub_field('fo_link');
      while ( have_rows('fo_footer','option') ) : the_row();?>
        <a href="<?php the_sub_field('fo_link');?>" target="_blank"><i class="fa <?php the_sub_field('fo_icon'); ?>"
         style="<?php the_sub_field('fo_icon_size'); ?>"<?php if( $link['target']=="_blank"){echo 'target="_blank"';} ?>></i></a>
      <?php
      endwhile;?>
    </div>
  <?php
  endif;?>
</footer>
  <?php wp_footer(); ?>
</body>
</html>
