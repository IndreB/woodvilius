<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <meta charset="<?php bloginfo('charset'); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title><?php echo get_the_title(); ?></title>
<?php wp_head(); ?>
</head>
<body>
  <header>
    <div class="header-menu-item">
      <a href="<?php echo home_url(); ?>"><?php bloginfo('name'); ?></a>
    </div>
    <ul class="nav">
      <li class="header-menu-item has-dropdown">
        <a class="menubar" href="#"><?php _e('Meniu'); ?><span> >></span></a>
        <ul class="dropdown">
             <?php wp_nav_menu(array(
              'theme_location' =>'primary-navigation',
              'container' => false
            )); ?>
        </ul>
      </li>
      <li class="header-menu-item">
        <a href="<?php echo home_url(); ?>">titulinis</a>
      </li>
      <li class="header-menu-item">
        <a href="<?php echo home_url(); ?>#about">APIE</a>
      </li>
      <li class="header-menu-item">
        <a href="<?php echo home_url(); ?>#map">KONTAKTAI</a>
      </li>
      <li class="header-menu-item">
        <a href="<?php echo home_url(); ?>#message">SUSISIEKTI</a>
      </li>
    </ul>
    <div class="burger">
      <div></div>
      <div></div>
      <div></div>
    </div>
  </header>
