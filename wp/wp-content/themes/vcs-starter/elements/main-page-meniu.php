
  <?php
  if( have_rows('hp_meniu') ):?>
  <div class="main-meniu"><?php
    while ( have_rows('hp_meniu') ) : the_row();
      $link = get_sub_field('hpm_link');?>
      <div class="<?php the_sub_field('hpm_class')?>">
        <div>
          <a href="<?php echo $link["url"];?>"><?php the_sub_field('hpm_title')?></a>
        </div>
      </div>
    <?php
    endwhile;?>
  </div>
<?php
endif;
?>
