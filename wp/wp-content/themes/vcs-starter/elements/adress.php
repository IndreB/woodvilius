<div id="contacts" class="contact">
  <div id="map">
    <?php $map=get_field('hp_map'); ?>
    <iframe src="//www.google.com/maps/embed/v1/place?q=<?php echo urlencode($map['address']); ?>
      &zoom=17 &key=AIzaSyBxjb3q9boXpQObB8aBShZ4OfEYiJIme1E"  width="600" height="450" frameborder="0" style="border:0" allowfullscreen>
    </iframe>
    <?php
     if( have_rows('hp_adress') ):?>
    <div class="contacts-bord">
     <ul>
       <?php
        while ( have_rows('hp_adress') ) : the_row();?>
         <li>
           <p><?php the_sub_field('hp_contact_type');?></p>
           <p><?php the_sub_field('hp_contact_content');?></p>
         </li>
        <?php
        endwhile;?>
      </ul>
     </div>
    </div>
    <?php
   endif;?>
</div>
