<?php
/* Template Name: Product page */

get_header();

if( have_rows('product_info') ):
  $i = 0;?>
 	<div class="products">
    <?php
    while ( have_rows('product_info') ) : the_row();
      $image = get_sub_field('pr_image');?>
        <div class="product-item">
         <button data-product = "foto<?php echo $image['ID']; ?>" class="zoom">
           <strong>+</strong>
         </button>
         <img src="<?php echo $image['url']; ?>" alt="<?php the_sub_field('pr_name'); ?>">
         <h5><?php the_sub_field('pr_name'); ?></h5>
        </div>
        <div class="issoksta foto<?php echo $image['ID']; ?>">
          <div class="pagrindas"></div>
          <div class="cover">
            <img src="<?php  echo $image['url']; ?>" alt="<?php the_sub_field('pr_name'); ?>">
            <p><?php the_sub_field('pr_description'); ?></p>
          </div>
         </div>
      <?php
      $i++;
    endwhile;?>
  </div>
<?php
endif;

get_footer();
?>
